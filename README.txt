Overview:
--------
The movie review module allows a site to publish movie reviews. 

Each review is a node with some particular fields. This module
was created based on book review module. 


Features:
--------
 - Provides fields for movie title, cover, actors, director, writer, 
 genre,  year of production, country, language, runtime, media, imdb, 
 rating, price, synopsis, location and review.
 - Supports unlimited number of actors.
 - Supports unlimited number of web links.


Installation and configuration:
------------------------------
Please refer to the INSTALL file for complete installation and 
configuration instructions.


Upgrade (4.6 --> 4.7)
---------------------
Just replace the old moviereview.module and moviereview.css files by the new ones.


Requires:
--------
 - Drupal 4.7.x


Credits:
-------
 - Written by Emiliano Ledo <emiliano@webinteligente.com.br>
 - This module is based on book review module written by Jeremy Andrews
 [even this readme file is copied from Jeremy's one!] ;-)