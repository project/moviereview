CREATE TABLE moviereview (
  nid serial NOT NULL,
  movietitle varchar(255) NOT NULL default '',
  release varchar(255) NOT NULL default '0',
  tvseries integer NOT NULL default '0',
  cover varchar(255) NOT NULL default '',
  genre varchar(255) NOT NULL default '',
  director varchar(255) NOT NULL default '',
  writer varchar(255) NOT NULL default '',
  runtime varchar(255) NOT NULL default '0',
  country varchar(255) NOT NULL default '',
  language varchar(255) NOT NULL default '',
  imdb varchar(255) NOT NULL default '',
  media varchar(255) NOT NULL default '',
  price varchar(255) NOT NULL default '',
  rating integer NOT NULL default '0',
  review text,
  location text,
  PRIMARY KEY (nid)
);

CREATE INDEX movietitle_idx ON moviereview (movietitle);


CREATE TABLE moviereview_actors (
  aid serial NOT NULL,
  nid integer NOT NULL default '0',
  actor varchar(255) NOT NULL default '',
  weight integer default '0',
  PRIMARY KEY (aid)
);

CREATE INDEX moviereview_actors_idx ON moviereview_actors (nid);



CREATE TABLE moviereview_links (
  lid serial NOT NULL,
  nid integer NOT NULL default '0',
  movielink varchar(255) NOT NULL default '',
  description varchar(255) NOT NULL default '',
  weight integer default '0',
  PRIMARY KEY (lid)
);

CREATE INDEX moviereview_links_idx ON moviereview_links (nid);

