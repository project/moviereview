<?php
/**
 * Copy this file to your drupal installation root and run from a web browser
 *
 * BACK UP YOUR DATABASE FIRST!
 */


include_once 'includes/bootstrap.inc';
include_once 'includes/common.inc';


if (module_exist('moviereview')) {
  db_query("ALTER TABLE {moviereview} DROP synopsis");
}

print 'Your database must be up-to-date now.';
?>