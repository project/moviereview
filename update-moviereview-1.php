<?php
/**
 * Copy this file to your drupal installation root and run from a web browser
 *
 * BACK UP YOUR DATABASE FIRST!
 */


include_once 'includes/bootstrap.inc';
include_once 'includes/common.inc';


if (module_exist('moviereview')) {
  db_query("update {node} n inner join {moviereview} m on n.nid = m.nid set n.body = m.synopsis where n.nid = m.nid");
}

print "Your database must be up-to-date now. Please, check if everything is ok and only when you're sure about it, run the file update-moviereview-2.php .<br /><br /> The fastest way for you to make sure everything's fine is displaying some movie reviews and also doing a search in your site using movie review's words. If the search returns some phrases of the movie's synopsis, it's ok. If the search returns only '...' for a movie that has synopsis, the update failed.";
?>